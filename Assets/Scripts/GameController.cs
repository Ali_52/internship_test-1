﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
    /// <summary>
    /// # el3ab internship test
    /// 
    ///   Write code to sortd the circles "children's under gameobject "container" in cirlce form around sort btn when user click on sort button
    /// 
    ///   NOTE : - feel free to change the following code as you see fit if you want 
    ///          - keep you code organized and commented
    /// 
    ///     Good Luck 
    ///   
    /// </summary>

    //container is filled with small circles 
    //  small circles is the game object's  that shall be sorted in form of circle shape around the Button
    public GameObject container; 


    // onClick on ("sort button") this function will be called and excuted
    public void SortBtn() 
    {
        //loop to scan each child inside the container
        for (int i = 0; i < container.transform.childCount; i++)
        {
            //  set child position depending on it's index 
           container.transform.GetChild(i).transform.localPosition = calculatePosition(i);

        }
    }


    // function responsible to calculate position 
    // Write your Code Here  
    //VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
    private Vector2 calculatePosition(int index)
    {

       


        return new Vector2(index, index);
    }
    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    // NOTE AGAIN : you have permission to change code as you see fit if you want 



}
